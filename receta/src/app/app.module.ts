import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ShellComponent } from './main/shell/shell/shell.component';
import { MainModule } from './main/main.module';


@NgModule({
  imports: [
    BrowserModule,
    MainModule
  ],
  providers: [],
  bootstrap: [ShellComponent]
})
export class AppModule { }
