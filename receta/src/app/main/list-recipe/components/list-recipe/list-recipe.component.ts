import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { RecipesService } from '../../services/recipes.service';
import { Recipes } from '../../modules/recipes';
import { ResponseRecipe } from '../../modules/responseRecipe';
import { Constants } from '../../constants/constants';

@Component({
  selector: 'app-list-recipe',
  templateUrl: './list-recipe.component.html',
  styleUrls: ['./list-recipe.component.css']
})
export class ListRecipeComponent implements OnInit, OnChanges {

  @Input() inTitle : string;
  public listRecipe: Recipes[];

  constructor(private recipesService: RecipesService) {
    this.listRecipe = new Array();
   }

  ngOnChanges() {
    this.getRecipes(this.inTitle);
    console.log(this.listRecipe);

  }
  ngOnInit() {
    this.getRecipes();

  }
  public imagen(url) {
    if( url === Constants.VACIO) {
      return Constants.IMG_DEFAULT;
    }
    return url;
  }
  

  private getRecipes(title? : string) {
    this.listRecipe = new Array();
    this.recipesService.getRecipe(title).subscribe (datos => {
      datos.results.map( recipe => {
        this.listRecipe.push(recipe);
      })
    }, err => {
      console.log("Error",err);
    });
  }


}
