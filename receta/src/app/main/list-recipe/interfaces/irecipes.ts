import { Observable } from "rxjs/Observable";
import { Recipes } from "../modules/recipes";
import { ResponseRecipe } from "../modules/responseRecipe";

export interface Irecipes {

    getRecipe(title: String) : Observable<ResponseRecipe>;
}
