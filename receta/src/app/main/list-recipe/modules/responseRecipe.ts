import { Recipes } from "./recipes";

export class ResponseRecipe {
    title: string;
    version: string;
    href: string;
    results: Recipes[];
}