import { Injectable } from '@angular/core';
import {HttpClient, HttpParams  } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { Irecipes } from '../interfaces/irecipes';
import { ResponseRecipe } from '../modules/responseRecipe';
import { Constants } from '../constants/constants';

@Injectable()
export class RecipesService implements Irecipes{

  constructor(private http: HttpClient) {}

  getRecipe(title?: string): Observable<ResponseRecipe> {
    
    const params = this.getParamHttp(title);

   return this.http.get<ResponseRecipe>(Constants.URL_CORS + Constants.URL, { params});
  }

  getParamHttp(params) {
    if(typeof(params) !== 'undefined') {
      return new HttpParams().set(Constants.PARAM_SEARCH, params);
    }
    return new HttpParams().set(Constants.PARAM_SEARCH, Constants.VACIO);
  }
 
}
