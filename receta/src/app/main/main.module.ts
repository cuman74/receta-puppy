import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { CommonModule } from '@angular/common';
import { BarSearchComponent } from './search/components/bar-search/bar-search.component';
import { ShellComponent } from './shell/shell/shell.component';
import { ListRecipeComponent } from './list-recipe/components/list-recipe/list-recipe.component';
import {HttpClientModule} from '@angular/common/http';
import { RecipesService } from './list-recipe/services/recipes.service';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  declarations: [
     BarSearchComponent,
     ShellComponent, 
     ListRecipeComponent
    ],
    providers : [RecipesService]
})
export class MainModule { }
