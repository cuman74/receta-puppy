import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-bar-search',
  templateUrl: './bar-search.component.html',
  styleUrls: ['./bar-search.component.css']
})
export class BarSearchComponent implements OnInit {

  @Output() outTitle = new EventEmitter<string>();
  public form: FormGroup;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.buildForm();
  }

  private buildForm() {
    this.form = this.formBuilder.group({
      recipe: ['',Validators.required]
    });
  }

  sendTitleRecipe(title : string) {
   this.outTitle.emit(title);
  }


}
