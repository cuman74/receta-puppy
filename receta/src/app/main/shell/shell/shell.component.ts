import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.css']
})
export class ShellComponent implements OnInit {

  public title: string;
  constructor() { }

  ngOnInit() {
  }
  getTitleRecipe(event: string){
    this.title = event;
  }

}
